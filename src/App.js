import React, {Component, Fragment} from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import Header from "./components/Header/Header";
import News from "./containers/News/News";
import NewPost from "./containers/NewPost/NewPost";
import Post from "./containers/Post/Post";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Header />
                <main className="container">
                    <Switch>
                        <Route path="/" exact component={News}/>
                        <Route path="/add-new-post" component={NewPost}/>
                        <Route path="/post/:id" component={Post}/>
                    </Switch>
                </main>
            </Fragment>
        );
    }
}

export default App;
