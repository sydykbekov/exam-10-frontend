import {ERROR_REQUEST, FETCH_COMMENTS_SUCCESS, FETCH_NEWS_SUCCESS, FETCH_POST_SUCCESS, START_REQUEST} from "./action";

const initialState = {
    news: [],
    comments: [],
    checkedPost: {},
    loading: false
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case START_REQUEST:
            return {...state, loading: true};
        case ERROR_REQUEST:
            return {...state, loading: false};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, comments: action.comments, loading: false};
        case FETCH_NEWS_SUCCESS:
            return {...state, news: action.news, loading: false};
        case FETCH_POST_SUCCESS:
            return {...state, checkedPost: action.checkedPost, loading: false};
        default:
            return state;
    }
};

export default reducer;