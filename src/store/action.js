import axios from 'axios';

export const START_REQUEST = 'START_REQUEST';
export const ERROR_REQUEST = 'ERROR_REQUEST';
export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';

export const startRequest = () => {
    return {type: START_REQUEST};
};

export const errorRequest = () => {
    return {type: ERROR_REQUEST};
};

export const fetchNewsSuccess = news => {
    return {type: FETCH_NEWS_SUCCESS, news};
};

export const fetchCommentsSuccess = comments => {
    return {type: FETCH_COMMENTS_SUCCESS, comments};
};

export const fetchPostSuccess = checkedPost => {
    return {type: FETCH_POST_SUCCESS, checkedPost};
};

export const getNews = () => {
    return dispatch => {
        dispatch(startRequest());
        axios.get('news').then(response => {
            dispatch(fetchNewsSuccess(response.data));
        }, error => {
            dispatch(errorRequest());
        });
    };
};

export const deletePost = (id) => {
    return dispatch => {
        axios.delete(`news/${id}`).then(() => {
             dispatch(getNews());
        }, error => {
            dispatch(errorRequest());
        });
    };
};

export const addPost = (postData, history) => {
    return dispatch => {
        dispatch(startRequest());
        axios.post('news', postData).then(() => {
            history.push('/');
        }, error => {
            dispatch(errorRequest());
        });
    };
};

export const getPost = id => {
    return dispatch => {
        dispatch(startRequest());
        axios.get(`news/${id}`).then(response => {
            dispatch(fetchPostSuccess(response.data[0]));
        }, error => {
            dispatch(errorRequest());
        });
    };
};

export const getComments = id => {
    return dispatch => {
        dispatch(startRequest());
        axios.get(`comments?news_id=${id}`).then(response => {
            dispatch(fetchCommentsSuccess(response.data));
        }, error => {
            dispatch(errorRequest());
        });
    };
};

export const sendComment = comment => {
    return dispatch => {
        dispatch(startRequest());
        axios.post('comments', comment).then(() => {
            dispatch(getComments(comment.id));
        });
    };
};

export const deleteComment = comment => {
    return dispatch => {
        dispatch(startRequest());
        axios.delete(`comments/${comment.id}`).then(() => {
            dispatch(getComments(comment.news_id));
        });
    };
};
