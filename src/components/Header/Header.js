import React from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const Header = () => (
    <Navbar>
        <Navbar.Header>
            <Navbar.Brand>
                <LinkContainer to="/" exact><a>News</a></LinkContainer>
            </Navbar.Brand>
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav pullRight>
                <LinkContainer to="/add-new-post">
                    <NavItem>Add new post</NavItem>
                </LinkContainer>
            </Nav>
        </Navbar.Collapse>
    </Navbar>
);

export default Header;