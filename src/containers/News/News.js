import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {deletePost, getNews} from "../../store/action";
import {Button, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import './News.css';
import Preloader from '../../assets/Preloader.gif';

const apiURL = 'http://localhost:8000/uploads/';

class News extends Component {
    componentDidMount() {
        this.props.getNews();
    }
    render() {
        return (
            <Fragment>
                <img src={Preloader} alt="preloader" className="preloader" style={{display: this.props.loading ? 'block' : 'none'}} />
                {this.props.news.map(post => (
                    <Panel key={post.id}>
                        <Panel.Body>
                            {post.image && <img style={{width: '100px', height: '100px'}} src={apiURL + post.image} alt="post-img"/>}
                            <h4>
                                {post.title}
                            </h4>
                            <span style={{marginRight: '20px'}}>
                            {post.date}
                        </span>
                            <Link to={'/post/' + post.id}>Read Full Post</Link>
                            <Button className="btn-delete" bsStyle="danger" onClick={() => this.props.deletePost(post.id)}>Delete</Button>
                        </Panel.Body>
                    </Panel>
                ))}
            </Fragment>

        )
    }
}

const mapStateToProps = state => {
    return {
        news: state.news,
        loading: state.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getNews: () => dispatch(getNews()),
        deletePost: id => dispatch(deletePost(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(News);