import React, {Component, Fragment} from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup, PageHeader} from "react-bootstrap";
import {connect} from "react-redux";
import {addPost} from "../../store/action";

class NewPost extends Component {
    state = {
        title: '',
        content: '',
        image: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData, this.props.history);
    };


    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };


    render() {
        return (
            <Fragment>
                <PageHeader>New post</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>
                    <FormGroup controlId="postTitle">
                        <Col componentClass={ControlLabel} sm={2}>
                            Title
                        </Col>
                        <Col sm={10}>
                            <FormControl
                                type="text" required
                                placeholder="Enter post title"
                                name="title"
                                value={this.state.title}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="postContent">
                        <Col componentClass={ControlLabel} sm={2}>
                            Content
                        </Col>
                        <Col sm={10}>
                            <FormControl
                                type="text" required
                                placeholder="Enter post text"
                                name="content"
                                value={this.state.content}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="postImage">
                        <Col componentClass={ControlLabel} sm={2}>
                            Image
                        </Col>
                        <Col sm={10}>
                            <FormControl
                                placeholder="Add file"
                                type="file"
                                name="image"
                                onChange={this.fileChangeHandler}
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button bsStyle="primary" type="submit">Save</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSubmit: (postData, history) => dispatch(addPost(postData, history))
    }
};

export default connect(null, mapDispatchToProps)(NewPost);