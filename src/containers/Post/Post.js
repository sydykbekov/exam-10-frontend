import React, {Component} from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup, Panel} from "react-bootstrap";
import {deleteComment, getComments, getPost, sendComment} from "../../store/action";
import {connect} from "react-redux";
import './Post.css';
import Preloader from '../../assets/Preloader.gif';

const apiURL = 'http://localhost:8000/uploads/';

class Post extends Component {
    state = {
        author: '',
        comment: ''
    };

    componentDidMount() {
        this.props.getPost(this.props.match.params.id);
        this.props.getComments(this.props.match.params.id);
    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    send = event => {
        event.preventDefault();
        const comment = {...this.state, id: this.props.match.params.id};
        this.props.sendComment(comment);
    };

    render() {
        return (
            <Panel>
                <img src={Preloader} alt="preloader" className="preloader" style={{display: this.props.loading ? 'block' : 'none'}} />
                <Panel.Body style={{textAlign: 'center'}}>
                    {this.props.checkedPost.image && <img className="post-image" src={apiURL + this.props.checkedPost.image} alt="post-pic"/>}
                    <h2>{this.props.checkedPost.title}</h2>
                    <p style={{color: 'grey'}}>{this.props.checkedPost.date}</p>
                    <p>{this.props.checkedPost.content}</p>
                </Panel.Body>
                {this.props.comments.map(comment => (
                    <Panel key={comment.id} className="comment">
                        <Panel.Body>
                            <h4>{comment.author} wrote:</h4>
                            <p>{comment.comment}</p>
                            <Button bsStyle="danger" type="submit" onClick={() => this.props.deleteComment(comment)}>Delete</Button>
                        </Panel.Body>
                    </Panel>
                ))}
                <Form horizontal onSubmit={this.send}>
                    <FormGroup controlId="commentAuthor">
                        <Col componentClass={ControlLabel} sm={2}>
                            Author
                        </Col>
                        <Col sm={8}>
                            <FormControl
                                type="text"
                                placeholder="Enter your name"
                                name="author"
                                value={this.state.author}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="commentText">
                        <Col componentClass={ControlLabel} sm={2}>
                            Comment
                        </Col>
                        <Col sm={8}>
                            <FormControl
                                type="text" required
                                placeholder="Enter your comment"
                                name="comment"
                                value={this.state.comment}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button bsStyle="primary" type="submit">Save</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Panel>
        )
    }
}

const mapStateToProps = state => {
    return {
        checkedPost: state.checkedPost,
        comments: state.comments
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getPost: id => dispatch(getPost(id)),
        getComments: id => dispatch(getComments(id)),
        sendComment: comment => dispatch(sendComment(comment)),
        deleteComment: comment => dispatch(deleteComment(comment))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Post);